import xml.etree.ElementTree as et
import argparse
#XTD:xjilec00
import collections
import sys

def enum(**input): # vlastni typ enum
    return type('Enum', (), input)

class Params:
    """trida pro praci s parametry"""
    attrGen = True
    distinct = False
    def __init__(self): # iniciliazace zakladnich promennych
        self.help = False
        self.input = ""
        self.output = ""
        self.header = ""
        self.etc = -1
        self.attrGen = True
        self.distinct = False
        self.shortGen = False

    def get(self): # specifikace povolenych parametru a prislusnych detailu
        parser = argparse.ArgumentParser(description='Prevedeni XML souboru na sekvenci SQL prikazu generujicich odpovidajici tabulky')
        parser.add_argument('--input', type=str, help='Vstupni soubor')
        parser.add_argument('--output', type=str, help='Vystupni soubor')
        parser.add_argument('--header', type=str, help='Zakomentovana hlavicka vlozena na zacatek')
        parser.add_argument('-a', action='store_false', help='Zakaze generovani sloupcu z atributu')
        parser.add_argument('-g', action='store_true', help='Vystupem bude zkracena verze XML')

        group = parser.add_mutually_exclusive_group() # pridani vzajemne se vylucujicich parametru
        group.add_argument('--etc', type=int, help='Maximalni pocet sloupcu vznikly ze stejnojmennych podelementu')
        group.add_argument('-b', action='store_true', help='Vice podelementu stejneho nazvu se povazuje za jediny')

        args = parser.parse_args() # ziskani parametru

        self.input = args.input # ulozeni do promennych
        self.output = args.output
        self.header = args.header
        Params.etc = args.etc
        self.attrGen = args.a
        Params.attrGen = self.attrGen
        self.distinct = args.b
        Params.distinct = self.distinct
        self.shortGen = args.g
        Params.shortGen = args.g

    def prepare_files(self): # priprava souboru pro dalsi praci
        if (self.input is not None): # pokud je zadan vstupni soubor
            try:
                self.inFile = open(self.input, 'r') # otevreme ho a priradime do promenne
            except IOError:
                sys.stderr.write("Nepodarilo se otevrit soubor") # pri chybe vypiseme do stderr
                sys.stderr.flush()
                exit(2)
        else:
            self.inFile = sys.stdin

        if (self.output is not None): # pokud je zadan vystupni soubor
            try:
                self.outFile = open(self.output, 'w') # otevreme ho pro zapis a priradime do promenne
            except IOError:
                sys.stderr.write("Nepodarilo se otevrit soubor") # pri chybe vzpiseme do stderr
                sys.stderr.flush()
                exit(3)
        else:
            self.outFile = sys.stdout

class XTD:
    """Hlavni trida programu obstaravajici ziskavani dat a prevod"""

    def __init__(self):
        self.dict_parents = {}
        self.dict_data = {}
        self.data_types = enum(BIT=0, INT=1, FLOAT=2, NVARCHAR=3, NTEXT=4) # deklarace hodnot enum typu


    def load_xml(self, inFile):
        self.tree = et.parse(inFile)
        self.root = self.tree.getroot()

    def iter_items(self, item):
        for child in item:
            self.get_item(item, child)
            self.iter_items(child)

    def get_item(self, parent, child):
        child_tag = child.tag.lower()
        self.dict_data.setdefault(child_tag, [[[],[]],[[],[]]])
        self.dict_data[child_tag][0][0].append(child.attrib)
        self.dict_data[child_tag][0][1].append(child.text)
        self.dict_data[child_tag][1][0].append(parent.tag.lower())
        self.dict_data[child_tag][1][1].append(parent.attrib)
    
    def update_type(self, old, new):
        k = 6


    def get_type(self, input, ret_int, is_attrib): # vrati hodnotu enum, bud int nebo retezec
        input = input.lower() # prevedeni na vstupu mala pismena
        if input == "true" or input == "false": # pokud je boolean
            if ret_int: # nastaveno vraceni int hodnoty
                return self.data_types.BIT
            else:
                return "BIT" # jinak vraceni retezce

        try:
            test = float(input)
            if ret_int:
                if test.is_integer():
                    return self.data_types.INT
                return self.data_types.FLOAT
            else:
                return "FLOAT"
        except ValueError: # nelze prevest na cislo v plovouci radove carce
            try:
                int(input)
                if ret_int:
                    return self.data_types.INT
                else:
                    return "INT"
            except ValueError: # nelze prevest na cele cislo
                if (is_attrib): # prevadeny prvek je atributem
                    if ret_int:
                        return self.data_types.NVARCHAR 
                    else:
                        return "NVARCHAR"
                else: # neni atribut
                    if ret_int:
                        return self.data_types.NTEXT
                    else:
                        return "NTEXT"

    def create_sql(self):
        self.print_dict = {} # slovnik pro ulozeni vystupu k tisku
        occurences = {} # slovnik pro pocet vyskytu ruznych elementu
        attributes = {} # slovnik pro atributy elementu
        self.global_attr = {}
        attr_types = [] # ulozeni typu atributu
        for child_tag in self.dict_data: # pro kazdy prvek ve slovniku s daty
            self.print_dict.setdefault(child_tag, "") # puvodni prvek je prazdnu retezec - mozno pridavat
            self.print_dict[child_tag] += 'CREATE TABLE ' + child_tag + '(\n' 
            self.dict_parents.setdefault(child_tag, set())
            self.global_attr.setdefault(child_tag, set())
            self.print_dict[child_tag] += '\tprk_' + child_tag + '_id INT PRIMARY KEY' + ',\n'

            for value in self.dict_data[child_tag][0][1]:  # prochazeni hodnot elementu
                if value is not None and not value.isspace(): # pokud neni hodnota prazdna
                    self.global_attr[child_tag].add("value")
                    self.print_dict[child_tag] += '\tvalue ' + self.get_type(value, False, False)  + ',\n'
                    break            

            oc_counter = 0 # pocitadlo vyskytu
            parent_tags = self.dict_data[child_tag][1][0] # jmena rodicovskych elementu
            parent_attr = self.dict_data[child_tag][1][1] # jejich atributy
            tmp_list = []
            for parent_tag_index in range(0, len(parent_tags)): # pro vsechny rodicovske elementy
                keys = parent_attr[parent_tag_index].keys() # nacteni klicu slovniku ve kterych jsou ulozeny atributy rodicu
                list_keys = list(keys) # prevedeni na list
                
                if (len(list_keys) >= 1): # pokud list neco obsahuje
                    attributes.setdefault(parent_tags[parent_tag_index], []) # puvodni prvek bude pole, mozno pridavat
                    for k in range(0, len(list_keys)): # pro kazdy klic v listu
                        if list_keys[k] not in attributes[parent_tags[parent_tag_index]]: # pokud jeste klic neni v listu atributu
                            tmp_dict = { list_keys[k] : self.get_type(parent_attr[parent_tag_index][list_keys[k]], True, True) }
                            for list_item in attributes[parent_tags[parent_tag_index]]:
                                list_item_keys = list(list_item.keys())
                                if (len(list_item_keys) > 0):
                                    for z in list_item_keys:
                                        if list_keys[k] == z:
                                            if (tmp_dict[z] > list_item[z]):
                                                list_item = tmp_dict;
                                        elif (z not in tmp_list):
                                            tmp_list.append(z)
                                            attributes[parent_tags[parent_tag_index]].append(tmp_dict)
                            if (len(attributes[parent_tags[parent_tag_index]]) == 0): # prvni pridani 
                                attributes[parent_tags[parent_tag_index]].append(tmp_dict)

                                

                for parent_tag_subindex in range(0, len(parent_tags)): # cyklus pro porovnani vsech prvku se vsemi
                    if (parent_tags[parent_tag_index] == parent_tags[parent_tag_subindex]): # pokud ma prvek z vyssiho cyklu stejny nazev 
                        if (parent_attr[parent_tag_index] == parent_attr[parent_tag_subindex]): # pokud jsou jejich atributy stejne, nazev i hodnota, porovnanavni slovniku
                            oc_counter += 1 # pricteme k pocitadlu vyskytu
                            occurences.setdefault(parent_tags[parent_tag_index], {}) # slovnik slovniku
                            occurences[parent_tags[parent_tag_index]][child_tag] = oc_counter # do hodnoty slovniku slovniku pridame hodnotu
                oc_counter = 0 # vynulovani pocitadla pred dalsim cyklem
            tmp_list = []
        
        if Params.attrGen:
            for value in attributes: # pro kazdy prvek v atributech
                if value in self.print_dict: # pokud je hodnota ve slovniku pro tisk
                    for k in attributes[value]: # pro kazdou hodnotu ve slovniku
                        k_keys = list(k.keys())

                        to_write = ""
                        if k[k_keys[0]] == 0:
                            to_write = 'BIT'
                        if k[k_keys[0]] == 1:
                            to_write = 'INT'
                        if k[k_keys[0]] == 2:
                            to_write = 'FLOAT'
                        if k[k_keys[0]] == 4:
                            to_write = 'NTEXT'
                        if k[k_keys[0]] == 3:
                            to_write = 'NVARCHAR'
                        """konec switche"""
                    
                        self.global_attr[child_tag].add(str(k_keys[0]))
                        self.print_dict[value] += '\t' + str(k_keys[0]) + ' ' + to_write  + ',\n'
        

        for item in occurences: # pro kazdy prvek ve vyskytech
            if item is not self.root.tag: # pokud polozka neni korenovy uzel
                for a in occurences[item]: # pro kazdou hodnotu ve slovniku
                    if Params.etc is not None:
                        if Params.etc >= 0 and occurences[item][a] <= Params.etc:
                            for index in range(1, occurences[item][a]+1): # zjisteni poctu vyskytu tohoto atributu
                                self.print_dict.setdefault(item, "") # puvodni hodnota je prazdny retezec - mozno pridavat
                                if (occurences[item][a] == 1) or Params.distinct: # pokud se polozka vyskytuje pouze jednou
                                    self.print_dict[item] += '\t' + a + '_id' + ' INT' + ',\n' # netiskneme index za nazvem
                                    self.dict_parents[item].add(a)
                                    if Params.distinct:
                                        break
                                else:
                                    self.dict_parents[item].add(a)
                                    self.print_dict[item] += '\t' + a + str(index) + '_id' + ' INT' + ',\n' # jinak index tisknut bude
                        else:
                            self.dict_parents[a].add(item)
                            self.print_dict[a] += '\t' + item + '_id' + ' INT' + ',\n'
                    else:
                        for index in range(1, occurences[item][a]+1): # zjisteni poctu vyskytu tohoto atributu
                            self.print_dict.setdefault(item, "") # puvodni hodnota je prazdny retezec - mozno pridavat
                            if (occurences[item][a] == 1) or Params.distinct: # pokud se polozka vyskytuje pouze jednou
                                self.print_dict[item] += '\t' + a + '_id' + ' INT' + ',\n' # netiskneme index za nazvem
                                self.dict_parents[item].add(a)
                                if Params.distinct:
                                    break
                            else:
                                self.dict_parents[item].add(a)
                                self.print_dict[item] += '\t' + a + str(index) + '_id' + ' INT' + ',\n' # jinak index tisknut bude

        for child_tag in self.dict_data: # pro kazdy prvek (tabulku)
            self.print_dict[child_tag] += ');\n' # vytiskneme ukoncovaci sekvenci znaku

    def is_relation(self, a, b):
        if b in self.dict_parents[a]:
            return True
        return False

    def transitivity(self, relation_str):
        iter = True
        while iter:
            iter = False
            for rel_a in self.xml_values:
                for rel_b in self.xml_values[rel_a]:
                    rel_val = self.xml_values[rel_a][rel_b]
                    if rel_val == "":
                        for rel_c in self.dict_parents:
                            if rel_c == rel_a or rel_b == rel_c:
                                continue

                            if (relation_str == 'N:M'):
                                try:
                                    if (self.xml_values[rel_a][rel_c] != '' and self.xml_values[rel_c][rel_b] != ''):
                                        self.xml_values[rel_a][rel_b] = relation_str
                                        try:
                                            self.xml_values[rel_b][rel_a] = relation_str
                                        except KeyError:
                                            iter = True
                                            continue
                                        iter = True
                                except KeyError:
                                    continue
                            else:
                                try:
                                    if (self.xml_values[rel_a][rel_c] == relation_str and self.xml_values[rel_c][rel_b] == relation_str):
                                        self.xml_values[rel_a][rel_b] = relation_str
                                        iter = True
                                except KeyError:
                                    continue

                            

    def create_xml(self):
        self.print_data = {}
        self.xml_values = {}
        for a in self.dict_parents:
            for b in self.dict_parents:
                self.xml_values.setdefault(a, {})[b] = ""

                if a == b:
                    self.xml_values[a][b] = '1:1'
                elif self.is_relation(a, b) and self.is_relation(b, a):
                    self.xml_values[a][b] = 'N:M'
                elif self.is_relation(a, b) and not self.is_relation(b, a):
                    self.xml_values[a][b] = 'N:1'
                elif self.is_relation(b, a) and not self.is_relation(a, b):
                    self.xml_values[a][b] = '1:N'
                else:
                    self.xml_values[a][b] = ''
      
        self.transitivity('1:N')
        self.transitivity('N:1')
        self.transitivity('N:M')

        for a in self.dict_parents:
            self.print_data.setdefault(a, '')
            self.print_data[a] += '<table name="' + a + '">\n'
            for b in self.dict_parents:
                self.print_data[a] += '\t<relation to="' + b + '" relation_type="'
                self.print_data[a] += self.xml_values[a][b] + '" />\n';
            self.print_data[a] += '</table>\n'
        
                
           




def main():
    params = Params() # inicilizace hlavni tridy pro praci s parametry
    params.get() # ziskani parametru a jejich ulozeni
    params.prepare_files() # na zaklade parametru priprava souboru

    xtd = XTD() # inicializace hlavni tridy
    xtd.load_xml(params.inFile) # nacteni xml
    xtd.iter_items(xtd.root) # projiti a nacteni hodnot do struktur
    xtd.create_sql() # vytvoreni sql

    for parent in xtd.dict_parents:
        for fk in xtd.dict_parents[parent]:
            if fk + '_id' == 'prk_' + parent + '_id':
                sys.exit(90)
            for attr in xtd.global_attr[parent]:
                if attr == 'prk_' + parent + '_id' or attr == fk + '_id':
                    sys.exit(90)


    if not Params.shortGen:
        if (params.header is not None):
            params.outFile.write('--' + params.header + '\n\n')
        for a in xtd.print_dict: # pro kazdou hodnotu ktera je urcena k tisku
            val = xtd.print_dict[a] 
            params.outFile.write(str(val).replace(',\n);', '\n);')) # tisk
    else:
        xtd.create_xml()
        params.outFile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        params.outFile.write('<tables>\n')
        for a in xtd.print_data:
            val = xtd.print_data[a] 
            params.outFile.write(str(val))
        params.outFile.write('</tables>\n')

main()